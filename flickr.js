class Flickr {
	constructor() {
		this.API_KEY = 'c3475658a3d8b4883942b292dd7c5da2';
		// this.defaultLatitude = 21.170686;
		// this.defaultLongitude = 94.858682;

		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(
				(position) => {
					this.latitude = parseInt(position.coords.latitude, 10);
					this.longitude = parseInt(position.coords.longitude, 10);
					// this.latitude = 21.170686;
					// this.longitude = 94.858682;
				},
				() => {
					ui.showAlert('Browser Not allowed to use Location', 'mx-auto .alert alert-danger');
					this.latitude = 21.170686;
					this.longitude = 94.858682;
				}
			);
		}
	}

	async getPhoto(searchItem) {
		let HTTP = await fetch(
			`https://shrouded-mountain-15003.herokuapp.com/https://flickr.com/services/rest/?api_key=${this
				.API_KEY}&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=2&per_page=20&lat=${this
				.latitude}&lon=${this.longitude}&text=${searchItem}`
		);

		const photo = await HTTP.json();
		return {
			photo
		};
	}
}
