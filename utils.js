function onInput(evt) {
	const userText = evt.target.value;

	if (userText !== '') {
		flickr.getPhoto(userText).then((data) => {
			let photosArr = data.photo.photos.photo;
			if (photosArr.length === 0) {
				ui.showAlert('Item Not found', '.alert alert-danger');
			} else {
				//ui.showSearchResults(photosArr[4]);
				for (let index = 0; index < photosArr.length; index++) {
					display(photosArr, index, 10000);
				}
			}
		});
	} else {
		//clear
		ui.clearInput();
	}
}

//Api was called each time the key was pressed and only wanted it be called when the
//key pressing was finshed
//Debounceing was the key
//CITEing https://medium.com/@TCAS3/debounce-deep-dive-javascript-es6-e6f8d983b7a1
//Citing https://codereview.stackexchange.com/questions/212016/debouncing-function-es6-version
function debounce(func, delay = 1000) {
	let timeoutId;
	return (...args) => {
		if (timeoutId) {
			clearTimeout(timeoutId);
		}
		timeoutId = setTimeout(() => {
			func.apply(null, args);
		}, delay);
	};
}

function display(arr, iteration, time) {
	setTimeout(function() {
		ui.showSearchResults(arr[iteration]);
	}, time * iteration);
}
