class UI {
	constructor() {
		this.photo = document.querySelector('#search-results');
	}

	showSearchResults(item) {
		//console.log(item);
		this.photo.innerHTML = `
            <div class="card card-body mb-3">
              <h3 class="card-title mx-auto">${item.title}</h3>
                <div class="row">
                    <div class="col-md-3 mx-auto">
                        <img class="img-fluid m-2" src="https://farm${item.farm}.staticflickr.com/${item.server}/${item.id}_${item.secret}_m.jpg"/>
                        <a href="https://www.flickr.com/photos/${item.owner}/${item.id}/" target="_blank" class="btn btn-outline-primary btn-block mb-4">View Profile</a>
                    </div>
                </div>
            </div>
        `;
	}

	showAlert(message, className) {
		this.clearAlert();

		const div = document.createElement('div');
		div.className = `${className}`;

		div.appendChild(document.createTextNode(message));

		const container = document.querySelector('.searchContainer');
		const search = document.querySelector('.search');
		container.insertBefore(div, search);

		setTimeout(() => {
			this.clearAlert();
		}, 3000);
	}

	clearAlert() {
		//onst currentAlert = document.querySelector('.alert');
		if (document.querySelector('.alert')) {
			currentAlert.remove();
		}
	}
	clearInput() {
		this.photo.innerHTML = '';
	}
}
