/*********************************************************************************************************
*                                         Requirements                                                   *
*                                                                                                        *
*      X = Get the geographic location from the browser
*      X = Construct the query URL
*      X = Use fetch to send the request to Flickr
*      X = Process the response data into an object
*      X = Use the values in the response object to construct an image source URL
*      X = Display the first image on the page
*      In response to some event(e.g.a button click or a setInterval), show the next image                *
*      in the collection                                                                                  *
***********************************************************************************************************/

const flickr = new Flickr();
const ui = new UI();

//Search input
const searchPhotos = document.querySelector('#search-item');

searchPhotos.addEventListener('input', debounce(onInput, 500));
